my_binomial <- rbinom(1000,8,0.5)
k <- c(0:8)
cdf <- rep(0.0,9)

for (i in 0:8){
  j <- i+1
  cdf[j] <- (sum(my_binomial<= i)/1000)
}

plot(k,cdf)


