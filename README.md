![Image](http://i.imgur.com/8AWyo3f.png)

# R Examples

### Project Wiki
* [Wiki](https://gitlab.com/senjuana/R-examples/wikis/home)

### Spanish
* [Acerca de](https://gitlab.com/senjuana/R-examples#acerca-de)
* [Requerimientos](https://gitlab.com/senjuana/R-examples#requerimientos)
* [Descargar](https://gitlab.com/senjuana/R-examples#descargar)
* [Contribuyendo](https://gitlab.com/senjuana/R-examples#contribuyendo)
* [Preguntas frecuentes](https://gitlab.com/senjuana/R-examples#preguntas-frecuentes)

### English
* [About](https://gitlab.com/senjuana/R-examples#about)
* [Requirements](https://gitlab.com/senjuana/R-examples#requirements)	
* [Getting the code](https://gitlab.com/senjuana/R-examples#getting-the-code)
* [Contributing](https://gitlab.com/senjuana/R-examples#contributing)
* [FAQ](https://gitlab.com/senjuana/R-examples#faq)



## Acerca de
Este es un repositorio publico que existe para poder guardar practicas de R.

## Requerimientos
Sistemas Operativos Soportados:
* Entorno de Desarrollo: MacOs, Gnu+Linux.

Requerimientos:
* R
* Una shell de R
* ó R studio


## Descargar
	$ git clone https://senjuana@gitlab.com/senjuana/R-examples.git
	$ cd R-examples
		

# Contribuyendo
Si encuentras un bug en el codigo o un issue, por favor notificame de manera privada en
[mi cuenta personal de twitter](https://twitter.com/senjuana).

Este proyecto sigue [code of merit](https://github.com/rosarior/Code-of-Merit). En este repositorio, me importa el codigo,
no opiniones personales o sentimientos. Espero tratar con  adultos.

Antes de enviar un pedazo de codigo por favor verifica que lo que envias funciona, yo no soy la persona que resolvera tus problemas con tu codigo.

# Preguntas Frecuentes

* **Cual es el punto de compartir tu codigo?**

    Mi unico interes es el de poder compartir mi projectos como  herramienta de aprendizaje.

* **Puedo utilizar este codigo en mis proyectos?**

    Siempre y cuando sigas los parametros de la licencia cualquiera puede utilizar o modiificar las implementaciones de este repositorio para cualquier proyecto personal.





## About
This is a Repository for save examples of R.

## Requirements
Supported operating systems:
* Development enviroments: MacOs, Gnu+Linux.

Requirements:
* R
* A R shell or
* R studio


## Getting the code
	$ git clone https://gitlab.com/senjuana/R-examples.git
	$ cd R-examples


# Contributing
If you found a bug on the code please let me know that by 
[my twitter account](https://twitter.com/senjuana).

This project follows [code of merit](https://github.com/rosarior/Code-of-Merit). 

Before of send a merge request please check your code, and see if exist a merge request of that subject before.

# FAQ

* **What it's the point of share your code?**
	
	This Repository as almost all my Public repositorys it's just for share a tool for learning

* **I can use this Code in my Projects?**

	As long as you follow the license of the repository, Sure.




